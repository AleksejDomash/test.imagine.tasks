package test.imagine.tasks.animate;

/**
 * Constraints
 * -----------
 * <p>
 * - speed will be between 1 and 10 inclusive
 * - init will contain between 1 and 50 characters inclusive
 * - each character in init will be '.' or 'L' or 'R'
 */
public class Animation {

    public static final char L = 'L';
    public static final char R = 'R';
    public static final char EMPTY = '.';

    private AnimationRenderer animationRenderer;

    public Animation(AnimationRenderer animationRenderer) {
        this.animationRenderer = animationRenderer;
    }

    public void animate(int speed, String init) {
        char[] chamber = init.toCharArray();
        char[] chambersBuffer = new char[chamber.length];
        copy(chamber, chambersBuffer);

        animationRenderer.render(chamber);
        while (!chamberIsEmpty(chamber)) {
            updateBuffer(chamber, chambersBuffer, speed);
            copy(chambersBuffer, chamber);
            animationRenderer.render(chamber);
        }
    }

    int chooseSpeedDirection(char command, int speed) {
        if (command == L) {
            return -speed;
        } else if (command == R) {
            return speed;
        } else {
            return 0;
        }
    }

    void updateBuffer(char[] chamber, char[] chambersBuffer, int speed) {
        char[] commandsBuffer = new char[2];

        for (int i = 0; i < chamber.length; i++) {
            if (chamber[i] != EMPTY) {
                setCommands(chamber, i, commandsBuffer);
                for (char command : commandsBuffer) {
                    if (isMoving(command)) {
                        int speedDirection = chooseSpeedDirection(command, speed);
                        int futurePosition = i + speedDirection;
                        if ((futurePosition > chambersBuffer.length - 1 || futurePosition < 0)) {
                            if(chambersBuffer[i] - command == R || chambersBuffer[i] - command == L){
                                chambersBuffer[i] = (char) (chambersBuffer[i] - command);
                            } else {
                                chambersBuffer[i] = EMPTY;
                            }
                        } else {
                            move(chambersBuffer, i, futurePosition, command);
                        }
                    }
                }
            }
        }
    }

    void copy(char[] chamberFrom, char[] chamberTo) {
        for (int i = 0; i < chamberFrom.length; i++) {
            chamberTo[i] = chamberFrom[i];
        }
    }

    void move(char[] chamber, int moveFrom, int moveTo, char command) {
        if (moveTo >= 0) {
            if (chamber[moveTo] == EMPTY) {
                chamber[moveTo] = command;
            } else {
                chamber[moveTo] = (char) (chamber[moveTo] + command);
            }
        }
        if (chamber[moveFrom] != EMPTY && (chamber[moveFrom] - command) >= 0) {
            if (chamber[moveFrom] - command == 0) {
                chamber[moveFrom] = EMPTY;
            } else {
                chamber[moveFrom] -= command;
            }
        } else {
            chamber[moveFrom] = EMPTY;
        }
    }

    boolean isMoving(char command) {
        return command > 0;
    }

    void setCommands(char[] chamber, int i, char[] commands) {
        if (chamber[i] == L) {
            commands[0] = L;
            commands[1] = 0;
        } else if (chamber[i] == R) {
            commands[0] = R;
            commands[1] = 0;
        } else if (chamber[i] == (R + L)) {
            commands[0] = R;
            commands[1] = L;
        } else {
            commands[0] = 0;
            commands[1] = 0;
        }
    }

    boolean chamberIsEmpty(char[] chamber) {
        boolean empty = true;
        for (char c : chamber) {
            empty = c == EMPTY;
            if (!empty) {
                break;
            }
        }
        return empty;
    }

    interface AnimationRenderer {
        char[] render(char[] buffer);
    }
}
