package test.imagine.tasks.animate;


public class NestedAnimationRenderer implements Animation.AnimationRenderer {
    private Animation.AnimationRenderer animationRenderer;

    public NestedAnimationRenderer(Animation.AnimationRenderer animationRenderer) {
        this.animationRenderer = animationRenderer;
    }

    @Override
    public char[] render(char[] animated) {
        return animationRenderer.render(animated);
    }
}
