package test.imagine.tasks.minibook;

import java.math.BigDecimal;

public interface Quote {
    String getId();
    BigDecimal getPrice();
    long getVolume();

    enum QuoteType {
        BID("B"),
        OFFER("O"), ;

        private String typeId;

        QuoteType(String typeId) {
            this.typeId = typeId;
        }

        public boolean is(String typeId) {
            return typeId.equals(this.typeId);
        }
    }
}
