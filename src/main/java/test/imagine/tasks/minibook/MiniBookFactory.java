package test.imagine.tasks.minibook;

import test.imagine.tasks.minibook.impl.MiniBookImpl;

public abstract class MiniBookFactory {
    public static <T extends Quote> MiniBook<T> createMiniBook(){
        return (MiniBook<T>) new MiniBookImpl();
    }
}
