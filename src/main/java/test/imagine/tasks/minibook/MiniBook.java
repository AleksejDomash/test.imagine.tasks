package test.imagine.tasks.minibook;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * A book includes all securities that the institution regularly buys and sells on
 * the stock market. A bid is the price that an institution is willing to buy and
 * an offer is the price that an institution is willing to sell.
 * <p>
 * Mini-Book receives quotes as input
 * A quote is a 5-tuple string containing: QuoteID/{B|O}/{N|U|D}/Price/Volume
 */
public interface MiniBook<T extends Quote> {
    /**
     * @param quote is a 5-tuple string containing: QuoteID/{B|O}/{N|U|D}/Price/Volume
     * @throws QuoteFormatValidationException if format is not correct
     */
    void receive(String quote);

    /**
     * @return map of quotes divided by quote type
     */
    Map<Quote.QuoteType, List<T>> dump();
}
