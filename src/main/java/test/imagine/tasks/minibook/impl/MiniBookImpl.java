package test.imagine.tasks.minibook.impl;

import test.imagine.tasks.minibook.MiniBook;
import test.imagine.tasks.minibook.Quote;
import test.imagine.tasks.minibook.QuoteFormatValidationException;

import java.math.BigDecimal;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MiniBookImpl implements MiniBook<QuoteImpl> {
    private static final Logger LOGGER = Logger.getLogger(MiniBookImpl.class.getName());

    private static final String OPERATION_NEW = "N";
    private static final String OPERATION_DELETE = "D";
    private static final String OPERATION_UPDATE = "U";

    private static final String OPERATION_DELETE_ID = "0";

    private Map<String, QuoteImpl> bids = new HashMap<>();
    private Map<String, QuoteImpl> offers = new HashMap<>();

    private List<QuoteImpl> sortedBids = new ArrayList<>();
    private List<QuoteImpl> sortedOffers = new ArrayList<>();

    private ProcessContext bidsProcessContext = new ProcessContext(new BidsComparator(), sortedBids, bids);
    private ProcessContext offersProcessContext = new ProcessContext(new OffersComparator(), sortedOffers, offers);

    void validate(String quote, String[] quoteParts) {
        if (quoteParts == null || quoteParts.length == 0) {
            throw new QuoteFormatValidationException(String.format(
                    "Quote %s has not correct separator / or is empty: %s", quote, Arrays.toString(quoteParts)
            ));
        }
        //TODO validation
    }

    String[] parse(String quote) {
        return quote.split("/");
    }

    @Override
    public void receive(String quote) {
        String[] quoteParts = parse(quote);
        validate(quote, quoteParts);
        long timestamp = System.nanoTime();
        String quoteType = quoteParts[1];
        processQuote(timestamp, quoteParts, chooseProcessContext(quoteType));
    }

    ProcessContext chooseProcessContext(String quoteType) {
        if (Quote.QuoteType.BID.is(quoteType)) {
            return bidsProcessContext;
        } else if (Quote.QuoteType.OFFER.is(quoteType)) {
            return offersProcessContext;
        } else {
            throw new IllegalArgumentException(String.format("There is no such type for quotes %s", quoteType));
        }
    }

    void processQuote(long timestamp, String[] quoteParts, ProcessContext processContext) {
        String operation = quoteParts[2];
        String id = quoteParts[0];

        QuoteImpl quoteObj = null;
        switch (operation) {
            case OPERATION_NEW:
                quoteObj = new QuoteImpl(id);
                processContext.quoteMap.put(quoteObj.getId(), quoteObj);
            case OPERATION_UPDATE:
                boolean oldObj;
                if (oldObj = (quoteObj == null)) {
                    quoteObj = processContext.quoteMap.get(id);
                }
                quoteObj.setPrice(new BigDecimal(quoteParts[3]));
                quoteObj.setVolume(Long.valueOf(quoteParts[4]));
                quoteObj.setTimestamp(timestamp);
                if (!oldObj) {
                    processContext.quoteList.add(quoteObj);
                }
                Collections.sort(processContext.quoteList, processContext.quoteComparator);
                break;
            case OPERATION_DELETE:
                if (OPERATION_DELETE_ID.equals(id)) {
                    processContext.quoteMap.clear();
                    processContext.quoteList.clear();
                } else {
                    if (processContext.quoteMap.containsKey(id)) {
                        QuoteImpl deletedQuoteObj = processContext.quoteMap.remove(id);
                        processContext.quoteList.remove(deletedQuoteObj);
                    } else {
                        LOGGER.log(Level.WARNING, "Delete operation for non-existing quote ID [{0}]", id);
                    }
                }
                break;
        }
    }

    @Override
    public Map<Quote.QuoteType, List<QuoteImpl>> dump() {
        HashMap<Quote.QuoteType, List<QuoteImpl>> quoteTypeHashMap = new HashMap<>();
        quoteTypeHashMap.put(Quote.QuoteType.BID, Collections.unmodifiableList(sortedBids));
        quoteTypeHashMap.put(Quote.QuoteType.OFFER, Collections.unmodifiableList(sortedOffers));
        return Collections.unmodifiableMap(quoteTypeHashMap);
    }

    class ProcessContext {
        Comparator<QuoteImpl> quoteComparator;
        List<QuoteImpl> quoteList;
        Map<String, QuoteImpl> quoteMap;

        public ProcessContext(Comparator<QuoteImpl> quoteComparator, List<QuoteImpl> quoteList, Map<String, QuoteImpl> quoteMap) {
            this.quoteComparator = quoteComparator;
            this.quoteList = quoteList;
            this.quoteMap = quoteMap;
        }
    }

    static int compareIfEqualPrices(QuoteImpl bidA, QuoteImpl bidB){
        int volumeResult = 0;
        if(bidA.getVolume() > bidB.getVolume()){
            volumeResult = -1;
        } else if (bidA.getVolume() < bidB.getVolume()){
            volumeResult = 1;
        }
        if(volumeResult == 0){
            int timestampResult = 0;
            if(bidA.getTimestamp() > bidB.getTimestamp()){
                timestampResult = -1;
            } else if (bidA.getTimestamp() < bidB.getTimestamp()){
                timestampResult = 1;
            }
            return timestampResult;
        }
        return volumeResult;
    }

    static class BidsComparator implements Comparator<QuoteImpl> {
        @Override
        public int compare(QuoteImpl bidA, QuoteImpl bidB) {
            int pricesBidResult = -bidA.getPrice().compareTo(bidB.getPrice());
            if(pricesBidResult == 0){
                return compareIfEqualPrices(bidA, bidB);
            }
            return pricesBidResult;
        }
    }

    static class OffersComparator implements Comparator<QuoteImpl> {
        @Override
        public int compare(QuoteImpl offerA, QuoteImpl offerB) {
            int pricesOfferResult = offerA.getPrice().compareTo(offerB.getPrice());
            if(pricesOfferResult == 0){
                return compareIfEqualPrices(offerA, offerB);
            }
            return pricesOfferResult;
        }
    }
}
